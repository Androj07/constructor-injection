package com.androj.injections.resource;

import com.androj.injections.boundary.TimeSupplier;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Created by andrzejhankus on 03/07/2017.
 */
@Path("main")
@Stateless
public class MainResource {


    TimeSupplier timeSupplier;

    @Inject
    public MainResource(TimeSupplier timeSupplier){
        this.timeSupplier = timeSupplier;
    }

    @GET
    public String getMessage(){
        return "Time is "+timeSupplier.current();
    }
}
