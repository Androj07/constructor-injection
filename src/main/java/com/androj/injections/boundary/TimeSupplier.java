package com.androj.injections.boundary;

import javax.ejb.Stateless;

/**
 * Created by andrzejhankus on 03/07/2017.
 */
@Stateless
public class TimeSupplier {

    public String current(){
        return String.valueOf(System.currentTimeMillis());
    }
}
