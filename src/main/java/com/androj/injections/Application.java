package com.androj.injections;

import javax.ws.rs.ApplicationPath;

/**
 * Created by andrzejhankus on 03/07/2017.
 */
@ApplicationPath("rest")
public class Application extends javax.ws.rs.core.Application{
}
